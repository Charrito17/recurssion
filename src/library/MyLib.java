package library;

public abstract class MyLib {

	/**
	 * Calculates the sum of elements in the given array.
	 * 
	 * @param arr
	 * @return
	 */
	public static int sum(int[] arr) {
		int sum = 0;
		for (int i = 0; i <= arr.length - 1; i++) {
			sum += arr[i];
		}

		return sum;
	}

	/**
	 * Calculates the following sum:
	 * 
	 * SUM(n) = 1 + 2 + 3 +... + n
	 * 
	 * @param n
	 * @return
	 */
	public static int sum_Iterative(int n) {
		int sum = 0;
		for (int i = 0; i <= n; i++) {
			sum += i;
		}

		return sum;
	}

	/**
	 * Calculates the following sum (recursively):
	 * 
	 * SUM(n) = 1 + 2 + 3 +... + n = = n + [1 + 2 + 3 + .. + (n-1)]
	 * 
	 * [Tail Recursion] SUM(3) = ?
	 * 
	 * 3 + SUM(2) 3 + 2 + SUM(1) 3 + 2 + 1
	 * 
	 * > 6
	 * 
	 * @param n
	 * @return
	 */
	public static int sum_Recursive1(int n) {
		if (n == 1) {
			return 1;
		} else {
			return (n + sum_Recursive1(n - 1));
		}

	}

	/**
	 * Calculates the following sum (recursively):
	 * 
	 * SUM(n) = 1 + 2 + 3 +... + n = = n + [1 + 2 + 3 + .. + (n-1)]
	 * 
	 * [Head Recursion] SUM(3, 0) = ?
	 * 
	 * SUM(2, 3) SUM(1, 5) SUM(0, 6)
	 * 
	 * > 6
	 * 
	 * @param n
	 * @return
	 */
	public static int sum_Recursive2(int n, int sum) {
		if (n == 1) {
			return sum;
		} else {
			return sum_Recursive2(n - 1, sum += n);
		}

	}

	/**
	 * Calculates the sum of the passed distances for the following situation: Can a
	 * rabbit goes from point A to point B, if in each move it jumps as long as half
	 * of the remaining distance?
	 * 
	 * 
	 * @param r
	 * @param n
	 * @return
	 */
	public static double rabbitJump_Iterative(double r, int n) {
		// n = number of jumps
		// r is the distance
		double sum = 0;
		for (int i = 0; i < n; i++) {
			r = r / 2;
			sum += r;
		}

		return sum;

	}

	/**
	 * Calculates the sum of the passed distances for the following situation: Can a
	 * rabbit goes from point A to point B, if in each move it jumps as long as half
	 * of the remaining distance?
	 * 
	 * 
	 * @param r
	 * @param n
	 * @return
	 */
	public static double rabbitJump_convergence(double r, int n) {

		if (n > 0) {
			r = r / 2;
			return r + rabbitJump_convergence(r, n - 1);
		} else {
			return 0;
		}

	}

	/**
	 * Calculates factorial of the given number:
	 * 
	 * n! = n X (n-1) X ... X 1
	 * 
	 * [Head Recursion] f(4) = ? 4 * f(3) 4 * (3 * f(2)) 4 * (3 * (2 * f(1))) 4 * (3
	 * * (2 * (1)))
	 * 
	 * > 24
	 * 
	 * @param n
	 * @return
	 */
	public static double factorial_Recursive1(double n) {
		if (n == 0) {
			return 1;
		} else {
			return (n * factorial_Recursive1(n - 1));
		}

	}

	/**
	 * Calculates factorial of the given number:
	 * 
	 * n! = n X (n-1) X ... X 1
	 * 
	 * [Tail Recursion] f(4) = ? 4 * f(3) 4 * (3 * f(2)) 4 * (3 * (2 * f(1))) 4 * (3
	 * * (2 * (1)))
	 * 
	 * > 24
	 * 
	 * @param n
	 * @return
	 */
	public static double factorial_Recursive2(double n, double fact) {
		if (n == 0) {
			return fact;
		} else {
			return factorial_Recursive2(n - 1, fact *= n);
		}

	}

	/**
	 * Calculates factorial of the given number:
	 * 
	 * n! = n X (n-1) X ... X 1
	 * 
	 * @param n
	 * @return
	 */
	public static double factorial_Iterative(double n) {
		int sum = 1;
		for (int i = 1; i <= n; i++) {
			sum *= i;
		}

		return sum;
	}

	/**
	 * Generates the Fibonacci sequence using recursion:
	 * 
	 * {1, 1, 2, 3, 5, 8, 11, 19, ...}
	 * 
	 * @param n
	 *            the index of the requested element from Fibonacci sequence
	 * @return the n-th element of the sequence.
	 */
	public static double fibonacci_Recursive(double n) {
		if (n == 0) {
			return 0;
		}
		if (n == 1) {
			return 1;
		} else {
			return fibonacci_Recursive(n - 1) + fibonacci_Recursive(n - 2);
		}

	}

	/**
	 * Generates the Fibonacci sequence using iteration:
	 * 
	 * {1, 1, 2, 3, 5, 8, 11, 19, ...}
	 * 
	 * @param n
	 *            the index of the requested element from Fibonacci sequence
	 * @return the n-th element of the sequence.
	 */
	public static double fibonacci_Iterative(double n) {
		if (n <= 1) {
			return n;
		}
		// these are pointers. that is why for loop starts at 2
		int firstNum = 1;
		int prevNum = 1;

		for (int i = 2; i < n; i++) {
			// temp = 1
			int temp = firstNum;
			// firstNum now equals 2
			firstNum = firstNum + prevNum;
			// prevNum = 1
			prevNum = temp;
		}
		return firstNum;
	}

}
