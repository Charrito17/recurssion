import library.AnimatedRecursion;
import library.MyLib;
import library.StdDraw;

//https://www.ibm.com/developerworks/websphere/techjournal/1307_col_paskin/1307_col_paskin.html
//https://introcs.cs.princeton.edu/java/23recursion/
//https://www.cs.cmu.edu/~adamchik/15-121/lectures/Recursions/recursions.html

/**
 * Practice for: Friday 6-Apr-2018
 * 
 * This project is implemented to let the students practice iterative and
 * recursive algorithms as well as some basic calculus.
 * 
 * This project contains both what we practiced and what is to be completed as
 * your homework.
 * 
 * 
 * @author Azim Ahmadzadeh - https://grid.cs.gsu.edu/~aahmadzadeh1/
 *
 */

public class Main {

	public static void main(String[] args) {

		double startTime;
		double stopTime;
		int input = 5;

		int[] arr = { 1, 2, 3, 4, 5 };
		int sum = MyLib.sum(arr);
		System.out.println("Sum: " + sum);

// Sum
		startTime = System.currentTimeMillis();
		System.out.println("Iterative sum =  " + MyLib.sum_Iterative(input));
		stopTime = System.currentTimeMillis();
		System.out.println("\t--> [" + (stopTime - startTime) + "]\n");

		startTime = System.currentTimeMillis();
		System.out.println("Recursive sum (tail) = " + MyLib.sum_Recursive1(input));
		stopTime = System.currentTimeMillis();
		System.out.println("\t--> [" + (stopTime - startTime) + "]\n");

		startTime = System.currentTimeMillis();
		System.out.println("Recursive sum (head) = " + MyLib.sum_Recursive2(input, 1));
		stopTime = System.currentTimeMillis();
		System.out.println("\t--> [" + (stopTime - startTime) + "]\n");

// Rabbit Jump
		startTime = System.currentTimeMillis();
		System.out.println("Rabbit Jump Iterative = " + MyLib.rabbitJump_Iterative(10, 5));
		stopTime = System.currentTimeMillis();
		System.out.println("\t--> [" + (stopTime - startTime) + "]\n");
		
		startTime = System.currentTimeMillis();
		System.out.println("Rabbit Jump Convergence = " + MyLib.rabbitJump_convergence(10, 5));
		stopTime = System.currentTimeMillis();
		System.out.println("\t--> [" + (stopTime - startTime) + "]\n");


// factorial Recursive
		startTime = System.currentTimeMillis();
		System.out.println("Recursive sum (head) = " + MyLib.factorial_Recursive1(input));
		stopTime = System.currentTimeMillis();
		System.out.println("\t--> [" + (stopTime - startTime) + "]\n");

		startTime = System.currentTimeMillis();
		System.out.println("Recursive sum (Tail) = " + MyLib.factorial_Recursive2(input, 1));
		stopTime = System.currentTimeMillis();
		System.out.println("\t--> [" + (stopTime - startTime) + "]\n");
		
		startTime = System.currentTimeMillis();
		System.out.println("Factorial Iterative = " +  MyLib.factorial_Iterative(input));
		stopTime = System.currentTimeMillis();
		System.out.println("\t--> [" + (stopTime - startTime) + "]\n");

// fibonacci
		startTime = System.currentTimeMillis();
		System.out.println("Recursive Sum (Head) = " + MyLib.fibonacci_Recursive(7));
		stopTime = System.currentTimeMillis();
		System.out.println("\t--> [" + (stopTime - startTime) + "]\n");

		startTime = System.currentTimeMillis();
		System.out.println("Recursive Sum (Interative) = " + MyLib.fibonacci_Iterative(7));
		stopTime = System.currentTimeMillis();
		System.out.println("\t--> [" + (stopTime - startTime) + "]\n");

		
//Extra Credit
		AnimatedRecursion.init();
		AnimatedRecursion.drawGoldenRatio_Recursive(5, 0, 0, 800);
		AnimatedRecursion.drawGoldenRatio_Iterative(5, 0, 0, 800);
		
	}

}